from crypt_handler import AESCipher
import sys

chandler = AESCipher('dsajdlksj')


def decrypt_file(file_path):
    """Receives a file path, decrypting it and returning the claertext content"""
    with open(file_path) as file_to_decrypt:
        decrypted_contents = chandler.decrypt(file_to_decrypt.read())
    f = open(file_path + '.decrypted', "w")
    f.write(decrypted_contents)
    f.close()


encrypted_file = sys.argv[1]
decrypt_file(encrypted_file)
