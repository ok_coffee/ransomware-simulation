import netifaces as ni
import socket
import netaddr
import ipaddress


class IPHandler():
    """A class to manipulate IP addresses and get IP info"""

    def __init__(self):
        self.ifaces = ni.interfaces()

    def is_local_subnet(self, ip_addr):
        """Checks an address to see if APIPA or localhost"""
        if ip_addr in netaddr.IPNetwork('127.0.0.0/8'):
            return False
        elif ip_addr in netaddr.IPNetwork('169.254.0.0/16'):
            return False
        else:
            return True

    def get_local_ip_subnets(self):
        """Get all local IP networks info"""
        results = []
        for iface in self.ifaces:
            ip_addresses = ni.ifaddresses(iface)
            if 2 in ip_addresses:
                ipinfo = ip_addresses[socket.AF_INET][0]
                address = ipinfo['addr']
                netmask = ipinfo['netmask']
                if self.is_local_subnet(address):
                    cidr = netaddr.IPAddress(netmask).netmask_bits()
                    net = netaddr.IPNetwork(address + '/' + str(cidr))
                    results.append(net)
        return results

    def smb_scan(self, ip_address):
        """Receives an IP address and scans for an open SMB port"""
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((ip_address, 445))
            # print("SMB port open")
            s.close()
            return True
        except:
            # print("SMB port closed")
            return False
